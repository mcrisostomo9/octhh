---
templateKey: sponsorship-page
heroSection:
  description: >-
    OC Tech Happy Hour is able to provide the wonderful event all thanks to our
    extraordinary sponsors. We wouldn’t be as successful as we have been without
    these amazing companies and people. We appreciate everything they provide
    our community.
  heroImage: /img/octhh_web_sponsorship.png
  title: Sponsorship
sponsorsSection:
  - logo: /img/18.12.2-official-superbad-logo-crop-png.png
    sponsor: Super Bad Media
    website: 'http://superbadmedia.com/'
  - logo: /img/jd-creativestudio_logo_black.png
    sponsor: jd creativestudio.com
    website: 'https://jd-creativestudio.com/'
  - logo: /img/ink-logo-blue-on-white-r-mini.png
    sponsor: INK
    website: 'https://inkdigitaltechnologies.com/'
  - logo: /img/lfzlogo1-2-.png
    sponsor: LearningFuze
    website: 'https://learningfuze.com/why-coding'
  - logo: /img/logosquare.png
    sponsor: Blue Light Media
    website: 'https://bluelight.media/'
  - logo: /img/asset-1.png
    sponsor: Eureka Building
    website: 'https://www.eurekabuilding.com/'
  - logo: /img/loftin-bedell-horiz.png
    sponsor: Loftin & Bedell Attorneys at Law
    website: 'https://www.loftinbedell.com/'
---

