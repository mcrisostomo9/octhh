---
templateKey: event-post
title: February 2019
heroSection:
  heroImage: /img/octhh_web_pastevents_feb.png
  title: February 2019
googleDriveLink: 'https://drive.google.com/drive/u/0/folders/1qJzdwSKgX8ZqmBZTbY0GVOUnqCwmd6bJ'
image: /img/octhh_pastevent_19_02.png
gallery:
  - image: /img/jumbotron.jpg
  - image: /img/img_20190225_110811.jpg
  - image: /img/chemex.jpg
date: 2019-04-12T01:35:20.609Z
---

