---
templateKey: event-post
title: January 2019
heroSection:
  heroImage: /img/octhh_web_pastevents_mar.png
  title: January 2019
googleDriveLink: 'https://drive.google.com/drive/u/0/folders/1aqM6NMy2abukViiOoFOuPWN2GU26jTjr'
image: /img/octhh_pastevent_19_01.png
gallery:
  - image: /img/4gm2hk19lsc21.jpg
  - image: /img/jumbotron.jpg
  - image: /img/img_20190225_110811.jpg
date: 2019-04-12T01:08:04.905Z
---

