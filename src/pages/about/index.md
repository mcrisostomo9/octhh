---
templateKey: about-page
heroSection:
  description: >-
    With a network now over 7,200 members strong, OC Tech Happy Hour is the fun
    place to embrace the startup culture, make new friends, find investors,
    invest in companies, learn about cutting edge technology and generally be a
    good human being.
  heroImage: /img/octhh_web_about.png
  title: What OC Tech is About
aboutSectionExtended: >-
  OC Tech Happy Hour is a community where there’s something for everyone whether
  you’re just beginning your startup journey or are a seasoned serial tech
  entrepreneur. 


  We host a variety of events focused on tech and entrepreneurship such as
  educational panels, industry relevant speaker series, social events for
  networking and more.


  Startups in our network have raised 10s of millions of dollars, our speakers
  have come from virtually every industry including Virtual Reality, Blockchain
  (Bitcoin), Nutrition, Information Technology, SaaS, and even Wine Making!


  At the end of the day, if we can connect entrepreneurs with investors,
  students with mentors, or job seekers with an employers we will have
  accomplished our mission.
boardOfDirectorsSection:
  - name: Hunter Lee Decker
    position: Founder
  - name: Dalip Jaggi
    position: President
  - name: Brian Walsh
    position: Vice President
  - name: Kevin Guo
    position: Treasurer
  - name: Amrit Jaggi
    position: Events Chair
  - name: Mikhail Alfon
    position: Social Chair
  - name: Errol Lawrence
    position: Organizer
  - name: Janna Cowper
    position: Lead Design Chair
  - name: Taylor Decker
    position: Event Experiences Chair
  - name: Mark Crisostomo
    position: Web Chair
  - name: Arash Maghbouleh
    position: Photo & Video Chair
  - name: Sam Vivona
    position: Design & Events Chair
---

