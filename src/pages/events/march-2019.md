---
templateKey: event-post
title: March 2019
heroSection:
  heroImage: /img/octhh_web_pastevents_feb.png
  title: March 2019
googleDriveLink: 'https://drive.google.com/drive/u/0/folders/1upp5wiYYTov5-RDyodJKuk4CcAGrlxQ5'
image: /img/octhh_pastevent_19_03.png
gallery:
  - image: /img/chemex.jpg
  - image: /img/blog-index.jpg
  - image: /img/img_20190225_110811.jpg
  - image: /img/flavor_wheel.jpg
  - image: /img/jumbotron.jpg
  - image: /img/products-grid2.jpg
date: 2019-04-12T01:36:49.839Z
---

