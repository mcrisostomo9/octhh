import React from "react";
import styled from "styled-components";
import SectionContainer from "../../components/SectionContainer/SectionContainer";
import SectionTitle from "../../components/SectionTitle/SectionTitle";
import { StaticQuery, graphql } from "gatsby";
import Img from "gatsby-image";
import bg from "../../img/pe_bg.png";
import mq from "../../utils/mq";

const StyledSectionContainer = styled(SectionContainer)`
  background: url(${bg}) no-repeat;
  background-size: cover;

  ${mq.a1200} {
    padding: 75px 40px;
  }
`;

const ItemContainer = styled.div`
  position: relative;
  cursor: pointer;

  :hover {
    filter: opacity(90%);
  }

  a {
    width: 100%;
    height: 100%;
  }
`;

const Title = styled.div`
  position: absolute;
  color: #fff;
  margin-left: auto;
  margin-right: auto;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  font-weight: bold;
`;

const EventsContainer = styled.div`
  display: grid;
  flex-direction: column;
  grid-template-columns: 1fr;
  grid-gap: 1rem;

  ${mq.a768} {
    grid-template-columns: 1fr 1fr;
  }

  ${mq.a1200} {
    grid-template-columns: 1fr 1fr 1fr;
    grid-gap: 2rem;
  }

  max-width: var(--max-width-desktop);
  margin: 0 auto;
`;

const PastEventsSection = () => (
  <StaticQuery
    query={graphql`
      query PastEventsPost {
        allMarkdownRemark(
          filter: { frontmatter: { templateKey: { eq: "event-post" } } }
          sort: { fields: [frontmatter___date], order: DESC }
        ) {
          edges {
            node {
              fields {
                slug
              }
              frontmatter {
                title
                googleDriveLink
                image {
                  childImageSharp {
                    fluid(maxWidth: 500, quality: 100) {
                      ...GatsbyImageSharpFluid_withWebp_tracedSVG
                    }
                  }
                }
              }
            }
          }
        }
      }
    `}
    render={data => {
      const events = data.allMarkdownRemark.edges;
      return (
        <StyledSectionContainer>
          <SectionTitle title="Past Events" />
          <EventsContainer>
            {events.map((e, index) => {
              const { title, image, googleDriveLink } = e.node.frontmatter;
              // const { slug } = e.node.fields;
              return (
                <ItemContainer key={index}>
                  <a
                    href={googleDriveLink}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <Img
                      fluid={image.childImageSharp.fluid}
                      style={{ height: "100%" }}
                      loading="lazy"
                    />
                    <Title>{title}</Title>
                  </a>
                </ItemContainer>
              );
            })}
          </EventsContainer>
        </StyledSectionContainer>
      );
    }}
  />
);

export default PastEventsSection;
