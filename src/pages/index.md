---
templateKey: index-page
heroSection:
  description: >-
    Entrepreneurship is hard.

    OC Tech Happy Hour is not. It’s the best place for investors, entrepreneurs,
    engineers, the technologically inclined and the creatively inspired.
  heroImage: /img/octhh_web_home.png
  title: A Place for Entrepreneurs
upcomingEventSection:
  date: Invalid date
  locationAddress: OC Tech Masquerade Gala
  locationName: The MET | Costa Mesa
  title: 'October 29, 2019 @ 6:00 PM'
agendaSection:
  active: true
  agendaItems:
    - timeRange: '6:00 PM - 6:45 PM'
      title: 'Venue Open, Networking & Appetizers'
    - timeRange: '6:45 PM - 7:00 PM'
      title: Ballerina Performance
    - timeRange: '7:00 PM - 7:15 PM'
      title: Welcoming Announcements
    - timeRange: '7:15 PM - 8:15 PM'
      title: Dinner & Live Jazz
    - timeRange: '8:15 PM - 8:30 PM'
      title: 2020 Preview Announcements
    - timeRange: '8:30 PM - 9:30 PM'
      title: Live Jazz & Mingle
  rsvpLink: 'https://www.eventbrite.com/e/oc-tech-masquerade-gala-tickets-69929590287'
featuredSection:
  active: true
  featuredItems:
    - biography: >-
        OTCHH’s end of year appreciation event to say a big THANK YOU for the
        support of the OC Tech Community.


        Sadly, this will be our last event of the year. So join us for a
        celebration of a fantastic 2019, and a look forward to an even more
        amazing 2020!
      image: /img/octhh_october_01.png
      name: Technology Inclined.
      position: Creatively Inspired.
locationSection:
  coordinates:
    latitude: '33.696654'
    longitude: '-117.830879'
  date: Oct 29th 19
  eventTimeRange: ' '
  parkingBlurb: Parking will be validated for this event.
  rsvpLink: 'https://www.eventbrite.com/e/oc-tech-masquerade-gala-tickets-69929590287'
  venueAddress: '535 Anton Blvd, Costa Mesa, CA 92626'
  venueName: The MET
---

